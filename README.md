# seven problems
----
 
Write a function called `reverseArray` that reverses an array in-place.
Do not use on the built-in `.reverse` method.
 
Usage:
 
    var array = [8, 6, 7, 5, 3, 0, 9];
    reverseArray(array) //=> [9, 0, 3, 5, 7, 6, 8]
    array //=> [9, 0, 3, 5, 7, 6, 8]
[answer](reversearray.js)

----
 
Write a method called `mySort` that sorts an array using any algorithm
without mutating the original array.
Do not use on the built-in `.sort` method.
 
Usage:
 
    var array = [8, 6, 7, 5, 3, 0, 9];
    array.mySort() //=> [0, 3, 5, 6, 7, 8, 9];
    array //=> [8, 6, 7, 5, 3, 0, 9]
----
 
Write a function called `getStockPrice` that takes as a parameter a
node-style asynchronous callback. Given the name of a company and the
availability of the following (theoretical) APIs, it should asynchronously
provide a value upon successful calculation of the stock price.
 
Available APIs:
 
    interface stockCode {
        get(name, callback);
    }
    
    interface stockPrice {
        get(code, callback);
    }
 
Usage:
 
    getStockPrice('Walmart, Inc.', function (err, value) {
        if (err) {
            console.error(err);
        } else {
            console.log('Walmart: ' + value);
        }
    });
 
----
 
Transform the following synchronous code to be asynchronous using ECMAScript 6
Promises. Assume you are in an ECMAScript 6 environment or otherwise have
`Promise` available to you.
 
Code:
 
    function makeBox(initial) {
        var value = initial;
        return {
            get: function () {
                return value;
            },
            set: function (newValue) {
                value = newValue;
            }
        }
    }
 
    var thing = makeBox(0);
    var v = thing.get();
    v += 1;
    thing.set(v);
    console.log(thing.get());
 
----
 
Given the following inert HTML5 code, add unobtrusive JavaScript (or otherwise
rewrite) such that when the user clicks the button, the counter increases from
0 to 1, then from 1 to 2, and so on. It should respond with visual feedback to
the user in such a way that he or she feels it is "immediate".
 
    <!DOCTYPE html>
    <html>
      <head>
        <title>Counter</title>
      </head>
      <body>
        <button class="counter-button">Click me!</button>
        <p class="counter-text">0</p>
      </body>
    </html>
[answer](counter.html)
 
----
 
Write an asynchronous function called `countLinks` that takes an array of URLs
and eventually resolves to an object.
Each property of said object should have a key representing a URL and a value
representing the number of hyperlinks found on that URL.
Use the `fetch` API (https://github.com/github/fetch).
`countLinks` should compute as much as possible in parallel.
 
Usage:
 
    countLinks(['http://www.google.com', 'http://space.vu', 'http://github.com']).then(function (o) {
        // note: numbers may have changed from when this example was written.
        assertEqual(o, {
            'http://www.google.com': 52,
            'http://space.vu': 33,
            'http://github.com': 180
        });
    });
[answer(not outputting object correctly)](countLinks.js)
 
----
 
Write a `Bag` collection (i.e. a `new`-able function) that has the following API:
 
    interface Bag {
        add(value); // add a value to the collection
        remove(value) : boolean; // try to remove a value from the collection
        has(value) : boolean; // return whether the value exists in the collection
        count(value) : number; // return the number of times `value` exists in the collection
        size() : number; // return the total number of items in the collection
        forEach(callback); // call `callback` for each item in the collection.
    }
 
Some points of note: `value` can be any type, and '0' and 0 should be seen as distinct values.
`forEach` can iterate in an arbitrary order.
 
Usage:
 
    var myBag = new Bag();
    myBag.size() === 0;
    myBag.count('x') === 0;
    myBag.add('x');
    myBag.size() === 1;
    myBag.count('x') === 1;
    myBag.count('y') === 0;
    myBag.has('x') === true;
    myBag.has('y') === false;
    myBag.remove('y') === false; // false because it wasn't removed
    myBag.remove('x') === true; // true because it was removed
    myBag.size() === 0;
    myBag.forEach(function (value) {
        doSomething(value);
    });
    
 [answer](bag.js)
