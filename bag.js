var Bag = function () {
	this.value = [];
	this.add = function(v) {
		this.value.push(v);
	};
	this.remove = function(v) {
		var index = this.value.indexOf(v);
		if (index > -1) {
			this.value.splice(index, 1);
			return true;
		} else {
			return false;
		}
	};
	this.has = function(v) {
		if (this.value.indexOf(v) > -1) {
			return true;
		} else {
			return false;
		}
	};
	this.count = function(v) {
		var counter = 0;
		for(var i = 0; i < this.value.length; i++) {
			if (v === this.value[i]) {
				counter += 1;
			}
		}
		return counter;
	};
	this.size = function() {
		return this.value.length + 1;
	};
	this.forEach = function(callback) {
		for (var i = 0; i < this.value.length; i++) {
			callback(this.value[i]);
		}
	};
}

