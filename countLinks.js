var fetch = require('node-fetch');
var _ = require("underscore");

var countLinks = function(inputArray) {
	var outputObj = {},
		link, counter = 0;
	for (var index in inputArray) {
		fetch(link = inputArray[index])
			.then(function(res) {
				counter += 1;
				outputObj[res.url] = counter;
				return res.text();
			}).then(function(body) {
				var re = /<a[^>]*>((?:.|\r?\n)*?)<\/a>/g;
				outputObj[(_.invert(outputObj))[counter]] = body.match(re).length;
				console.log(outputObj);
			});
	}
}

countLinks(['http://www.google.com', 'http://space.vu', 'http://github.com']); 
