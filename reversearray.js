function reverseArray(a) {
	var reversedArray = [];
	while (a.length > 0) {
		reversedArray.push(a.pop());
	}
	return reversedArray;
}
